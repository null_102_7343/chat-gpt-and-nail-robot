# 使用
## 方法1
```shell
mvn package
nohup java -Dgg.accessToken=sk-iuAQNsYaDxxxxxxxxxxxxxxxiTWV3eBjcCIw -Dgg.appKey=dixxxxxxxxxxxizutl -Dgg.appSecret=AgzTJ24w-zHZTlEQ2UG_1hx-M_G20xTxxxxxxxxxxxxxxxtzEVpwG2\
 -jar web/target/web-0.0.1-SNAPSHOT.jar &
```

## 方法2

正确填充下面的字段数据

```yaml
gg:
  # chat gpt 访问token
  accessToken: sk-iuAQNsYaDxxxxxxxxxxxxxxxiTWV3eBjcCIw
  # 钉钉 内部企业应用 key
  appKey: dixxxxxxxxxxxizutl
  # 钉钉 内部企业应用 secret
  appSecret: AgzTJ24w-zHZTlEQ2UG_1hx-M_G20xTxxxxxxxxxxxxxxxtzEVpwG2

```

执行
```shell
mvn package
nohup java -jar web/target/web-0.0.1-SNAPSHOT.jar &
```