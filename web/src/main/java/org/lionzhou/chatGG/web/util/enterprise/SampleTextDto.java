package org.lionzhou.chatGG.web.util.enterprise;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class SampleTextDto implements Serializable {
    String content;
}
