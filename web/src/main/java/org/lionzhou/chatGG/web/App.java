package org.lionzhou.chatGG.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@SpringBootApplication(scanBasePackages = "org.lionzhou.chatGG")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
