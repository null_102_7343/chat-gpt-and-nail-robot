package org.lionzhou.chatGG.web.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class DingAtUsers implements Serializable {

    String dingtalkId;
    String staffId;
}
