package org.lionzhou.chatGG.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "gg")
@Data
public class GGConfig {

    /**
     * ChatGPT 的token
     */
    String accessToken = "";

    /**
     * 钉钉 企业内部应用key
     */
    String appKey = "";
    /**
     * 钉钉 企业内部应用secret
     */
    String appSecret = "";

    /**
     * 钉钉 机器人hook地址
     */
    String dingDingHook = "";
    /**
     * 启用 配合 dingDingHook
     */
    Boolean enableDingDingHookRobot = false;
}
