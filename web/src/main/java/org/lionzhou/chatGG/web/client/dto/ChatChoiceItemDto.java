package org.lionzhou.chatGG.web.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class ChatChoiceItemDto implements Serializable {

    String text;
    Integer index;
    Integer logprobs;
    @JsonProperty("finish_reason")
    String finishReason;
}
