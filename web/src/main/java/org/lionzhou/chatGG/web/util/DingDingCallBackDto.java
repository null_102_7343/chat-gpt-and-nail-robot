package org.lionzhou.chatGG.web.util;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description
 * @Author ZhouLiangCheng
 * @Date 2023/2/10
 * @Version 1.0
 */
@Data
public class DingDingCallBackDto implements Serializable {
    String conversationId;
    String chatbotUserId;
    String msgId;
    String senderNick;
    Boolean isAdmin;
    Long sessionWebhookExpiredTime;
    Long createAt;
    String conversationType;
    String senderId;
    String conversationTitle;
    Boolean isInAtList;
    String sessionWebhook;
    TextDto text;
    String robotCode;
    String msgtype;
    List<DingAtUsers> atUsers;
}
